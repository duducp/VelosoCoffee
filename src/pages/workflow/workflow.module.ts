import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TranslateModule} from '@ngx-translate/core';
import { WorkflowPage } from './workflow';

@NgModule({
  declarations: [
    WorkflowPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkflowPage),
    TranslateModule.forChild()
  ],
})
export class WorkflowPageModule {}

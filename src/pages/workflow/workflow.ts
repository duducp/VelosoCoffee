import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

export interface Card {
  title: string;
  subtitle: string;
  image: string;
  namePage: string;
}

@IonicPage()
@Component({
  selector: 'page-workflow',
  templateUrl: 'workflow.html',
})
export class WorkflowPage {
  cards: Card[];

  constructor(public navCtrl: NavController) {
    this.cards = [
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE1_SUBTITLE',
        image: 'assets/img/workflow/nos-plantamos.jpg',
        namePage: 'WorkflowPlantedPage',
      },
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE2_SUBTITLE',
        image: 'assets/img/workflow/nos-cultivamos.jpg',
        namePage: 'WorkflowGrowPage',
      },
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE3_SUBTITLE',
        image: 'assets/img/workflow/nos-colhemos.jpg',
        namePage: 'WorkflowReapPage',
      },
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE4_SUBTITLE',
        image: 'assets/img/workflow/nos-selecionamos.jpg',
        namePage: 'WorkflowSelectPage',
      },
      {
        title: 'HOME_SLIDE5_TITLE',
        subtitle: 'HOME_SLIDE5_SUBTITLE',
        image: 'assets/img/workflow/nos-exportamos.jpg',
        namePage: 'WorkflowExportPage',
      },
    ];
  }

  openPage(namePage: string){
    this.navCtrl.push(namePage);
  }

  next() {
    this.navCtrl.push('ProductsPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}

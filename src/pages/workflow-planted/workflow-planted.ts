import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workflow-planted',
  templateUrl: 'workflow-planted.html',
})
export class WorkflowPlantedPage {

  constructor(public navCtrl: NavController) {
  }

}

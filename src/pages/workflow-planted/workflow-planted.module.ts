import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkflowPlantedPage } from './workflow-planted';

@NgModule({
  declarations: [
    WorkflowPlantedPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkflowPlantedPage),
    TranslateModule.forChild()
  ],
})
export class WorkflowPlantedPageModule {}

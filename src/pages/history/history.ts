import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

export interface Slide {
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  slides: Slide[];

  constructor(public navCtrl: NavController, translate: TranslateService) {
    translate.get([
      "HISTORY_SLIDE1_DESCRIPTION",
      "HISTORY_SLIDE2_DESCRIPTION",
      "HISTORY_SLIDE3_DESCRIPTION",
    ]).subscribe(
      (values) => {
        //console.log('Loaded values', values);
        this.slides = [
          {
            description: values.HISTORY_SLIDE1_DESCRIPTION,
            image: 'assets/img/history/1.jpg',
          },
          {
            description: values.HISTORY_SLIDE2_DESCRIPTION,
            image: 'assets/img/history/2.jpg',
          },
          {
            description: values.HISTORY_SLIDE3_DESCRIPTION,
            image: 'assets/img/history/3.jpg',
          }
        ];
      });
  }

  next() {
    this.navCtrl.push('RegionPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { IonicImageLoader } from 'ionic-image-loader';
import { CoffeeSamplesDetailsPage } from './coffee-samples-details';

@NgModule({
  declarations: [
    CoffeeSamplesDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CoffeeSamplesDetailsPage),
    TranslateModule.forChild(),
    IonicImageLoader
  ],
})
export class CoffeeSamplesDetailsPageModule {}

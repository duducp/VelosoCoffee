import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavParams, PopoverController, NavController, Slides } from 'ionic-angular';
import { GoogleMapsService } from '../../providers/google-maps/google-maps';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { Storage } from '@ionic/storage';
import { ErrorService } from '../../providers/error/error';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-coffee-samples-details',
  templateUrl: 'coffee-samples-details.html',
})
export class CoffeeSamplesDetailsPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  @ViewChild('slider') slider: Slides;
  protected params: any;
  protected currentIndex: number = 0;
  protected evaluated_sample: boolean = false;
  private dataRating: number;

  constructor(
    private navParams: NavParams,
    private maps: GoogleMapsService,
    private popoverCtrl: PopoverController,
    private navCtrl: NavController,
    private error: ErrorService,
    private imageLoaderConfig: ImageLoaderConfig,
    private storage: Storage,
    private translateService: TranslateService
  ) {
    this.params = this.navParams.get('sample');
    this.configImageLoader();
  }

  ionViewDidLoad() {
    this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement, this.params);
  }

  ionViewWillEnter() {
    this.storage.get('rating_sample')
    .then(data => {
      let objSample = this.params;
      let objRating = data;

      for (let i in objRating) {
        if (objRating[i].idSample == objSample._id) {
          this.evaluated_sample = true;
          this.dataRating = objRating[i];
          break;
        }
      }

    })
    .catch(err => {
      this.translateService.get([
        "ERROR_GET_STORAGE"
      ]).subscribe((values) => {
        this.error.handleError(values.ERROR_GET_STORAGE);
      });
    });
  }

  protected presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("PopoverPage", {code: this.params.code, index: this.navParams.get('index')});
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss((res) => {
      if (res) {
        if (res.type == 'update') {
          this.params = res.data;
        } else if (res.type == 'delete') {
          this.navCtrl.pop();
        }
      }
    });
  }

  private configImageLoader() {
    this.imageLoaderConfig.enableSpinner(true); // enable spinner
    this.imageLoaderConfig.setConcurrency(5); // set the maximum concurrent connections
    this.imageLoaderConfig.setFallbackUrl('assets/img/fallback.jpg'); // if images fail to load, display this image instead
    this.imageLoaderConfig.useImageTag(true); // use `<img>` tag by default
  }
  
  protected openPage(page: string, data: any) {
    this.navCtrl.push(page, {dataSample: data, dataRating: this.dataRating});
  }
  
  protected nextSlide() {
    this.slider.slideNext();
  }

  protected previousSlide() {
    this.slider.slidePrev();
  }

  protected onSlideChanged() {
    this.currentIndex = this.slider.getActiveIndex();
  }
}

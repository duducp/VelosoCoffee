import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ErrorService } from '../../../providers/error/error';
import { TranslateService } from '@ngx-translate/core';
import { SampleService } from '../../../providers/sample-coffee/sampleCoffeeService';

@IonicPage()
@Component({
  selector: 'page-popover',
  template: `
    <ion-content>
        <ion-list no-margin no-padding no-lines>
            <ion-item (click)="update(index, code)">
                <ion-icon name="refresh" item-start></ion-icon>
                {{ 'POPOVER_COFFEE_SAMPLE_DETAIL_BUTTON_RELOAD' | translate }}
            </ion-item>
            <ion-item (click)="delete(index)" color="danger">
                <ion-icon name="trash" item-start></ion-icon>
                {{ 'POPOVER_COFFEE_SAMPLE_DETAIL_BUTTON_DELETE' | translate }}
            </ion-item>
        </ion-list>
    </ion-content>
  `
})
export class PopoverPage {

  protected code: any;
  private loading: any;
  protected index: number;
  private translator: any;

  constructor(
    private navParams: NavParams,
    private storage: Storage,
    private viewCtrl: ViewController,
    private error: ErrorService,
    private translateService: TranslateService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private sampleService: SampleService
  ) {
    this.code = this.navParams.get('code');
    this.index = this.navParams.get('index');

    this.translateService.get([
      "COFFEE_SAMPLE_DETAIL_ERROR_GETSTORAGE",
      "BOTTON_OK",
      "COFFEE_SAMPLE_TITLE",
      "POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_YES",
      "POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_CANCEL",
      "POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_TITLE",
      "POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_DESCRIPTION",
      "POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_SUCCESS",
      "POPOVER_COFFEE_SAMPLE_DETAIL_SUCCESS_UPDATE",
      "LOADING",
    ]).subscribe((values) => {
      this.translator = values;
    });
  }

  protected delete(index) {
    this.getSamplesStorage()
    .then((resp) => {

      let alert = this.alertCtrl.create({
        title: this.translator.POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_TITLE,
        message: this.translator.POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_DESCRIPTION,
        buttons: [
          {
            text: this.translator.POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_CANCEL,
            role: 'cancel',
            handler: () => {
              this.viewCtrl.dismiss();
            }
          },
          {
            text: this.translator.POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_YES,
            handler: () => {
              resp.splice(index, 1);
              this.storage.set('samples', resp);
              this.showAlert(this.translator.POPOVER_COFFEE_SAMPLE_DETAIL_CONFIRM_SUCCESS);
        
              this.viewCtrl.dismiss({type: "delete"});
            }
          }
        ]
      });
      alert.present();

    });
  }

  protected update(index: number, code) {
    this.getSampleCoffee(code)
    .then((res) => {

      this.getSamplesStorage()
      .then((stor) => {
        stor.splice(index, 1, res[0]);
        this.storage.set('samples', stor);
        this.showAlert(this.translator.POPOVER_COFFEE_SAMPLE_DETAIL_SUCCESS_UPDATE);

        this.viewCtrl.dismiss({type: "update", data: res[0]});
      });

    });
  }

  private getSamplesStorage(): Promise<any> {
    return new Promise((resolve) => {
      this.storage.get('samples')
        .then((val) => {
          if (val)
            resolve(val);
        }, (err) => {
          this.error.handleError(this.translator.COFFEE_SAMPLE_DETAIL_ERROR_GETSTORAGE);
        });
    });
  }

  private getSampleCoffee(code: String): Promise<any> {
    this.showLoader();

    return new Promise((resolve) => {
      this.sampleService.get(code).then((res) => {
        this.loading.dismiss();
        resolve(res);
      }, (err) => {
        this.error.handleError(err);
        this.loading.dismiss();
      });
    });
  }

  private showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: this.translator.COFFEE_SAMPLE_TITLE,
      subTitle: message,
      buttons: [this.translator.BOTTON_OK]
    });
    alert.present();
  }

  private showLoader() {
    this.loading = this.loadingCtrl.create({
      content: this.translator.LOADING
    });

    this.loading.present();
  }
}

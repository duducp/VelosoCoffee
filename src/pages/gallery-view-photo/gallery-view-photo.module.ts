import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { IonicImageLoader } from 'ionic-image-loader';

import { GalleryViewPhotoPage } from './gallery-view-photo';

@NgModule({
  declarations: [
    GalleryViewPhotoPage,
  ],
  imports: [
    IonicImageLoader,
    IonicPageModule.forChild(GalleryViewPhotoPage),
    TranslateModule.forChild()
  ],
})
export class GalleryViewPhotoPageModule {}

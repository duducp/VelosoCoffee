import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, Slides } from 'ionic-angular';
import { ImageLoaderConfig } from 'ionic-image-loader';

@IonicPage()
@Component({
  selector: 'page-gallery-view-photo',
  templateUrl: 'gallery-view-photo.html',
})
export class GalleryViewPhotoPage {

  @ViewChild('slider') slider: Slides;
  protected photos: any;
  protected currentIndex: number;
  protected photo: any;

  constructor(
    private navParams: NavParams,
    private imageLoaderConfig: ImageLoaderConfig
  ) { }

  ionViewWillEnter() {
    this.configImageLoader();
    this.photos = this.navParams.get('photos');
    this.currentIndex = this.navParams.get('index');
    this.photo = this.navParams.get('photo');
  }

  protected nextSlide() {
    this.currentIndex++;
    this.photo = this.photos[this.currentIndex];
  }

  protected previousSlide() {
    this.currentIndex--;
    this.photo = this.photos[this.currentIndex];
  }

  private configImageLoader() {
    this.imageLoaderConfig.enableSpinner(true); // enable spinner
    this.imageLoaderConfig.setConcurrency(15); // set the maximum concurrent connections
    this.imageLoaderConfig.useImageTag(true); // use `<img>` tag by default
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workflow-select',
  templateUrl: 'workflow-select.html',
})
export class WorkflowSelectPage {

  constructor(public navCtrl: NavController) {
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkflowSelectPage } from './workflow-select';

@NgModule({
  declarations: [
    WorkflowSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkflowSelectPage),
    TranslateModule.forChild()
  ],
})
export class WorkflowSelectPageModule {}

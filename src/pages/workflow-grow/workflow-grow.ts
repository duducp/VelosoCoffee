import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workflow-grow',
  templateUrl: 'workflow-grow.html',
})
export class WorkflowGrowPage {

  constructor(public navCtrl: NavController) {
  }

}

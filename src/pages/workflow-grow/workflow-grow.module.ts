import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkflowGrowPage } from './workflow-grow';

@NgModule({
  declarations: [
    WorkflowGrowPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkflowGrowPage),
    TranslateModule.forChild()
  ],
})
export class WorkflowGrowPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workflow-reap',
  templateUrl: 'workflow-reap.html',
})
export class WorkflowReapPage {

  constructor(public navCtrl: NavController) {
  }

}

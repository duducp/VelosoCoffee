import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkflowReapPage } from './workflow-reap';

@NgModule({
  declarations: [
    WorkflowReapPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkflowReapPage),
    TranslateModule.forChild()
  ],
})
export class WorkflowReapPageModule {}

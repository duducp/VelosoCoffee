import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {IonicPageModule} from 'ionic-angular';
import {LongPressModule} from 'ionic-long-press';

import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import {HomePage} from './home';

@NgModule({
    declarations: [HomePage],
    imports: [
        IonicPageModule.forChild(HomePage),
        TranslateModule.forChild(),
        LongPressModule,
    ],
    providers: [
      BarcodeScanner
    ]
})
export class HomePageModule {
}
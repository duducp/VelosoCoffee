import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ErrorService } from '../../providers/error/error';
import { TranslateService } from "@ngx-translate/core";

export interface Slide {
  title: string;
  subtitle: string;
  image: string;
  namePage: string;
}

export interface Product {
  class: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  protected slides: Slide[];
  protected products: Product[];
  private optionsBarcode: BarcodeScannerOptions;
  protected translator: any;
  private longPressPageOpen: boolean;

  constructor(
    private navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    private error: ErrorService,
    private translateService: TranslateService
  ) {
    this.slides = [
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE1_SUBTITLE',
        image: 'assets/img/workflow/nos-plantamos.jpg',
        namePage: 'WorkflowPlantedPage',
      },
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE2_SUBTITLE',
        image: 'assets/img/workflow/nos-cultivamos.jpg',
        namePage: 'WorkflowGrowPage',
      },
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE3_SUBTITLE',
        image: 'assets/img/workflow/nos-colhemos.jpg',
        namePage: 'WorkflowReapPage',
      },
      {
        title: 'HOME_SLIDE_TITLE',
        subtitle: 'HOME_SLIDE4_SUBTITLE',
        image: 'assets/img/workflow/nos-selecionamos.jpg',
        namePage: 'WorkflowSelectPage',
      },
      {
        title: 'HOME_SLIDE5_TITLE',
        subtitle: 'HOME_SLIDE5_SUBTITLE',
        image: 'assets/img/workflow/nos-exportamos.jpg',
        namePage: 'WorkflowExportPage',
      },
    ];

    this.products = [
      {
        class: 'sublime',
        description: 'HOME_PRODUCT_SUBLIME',
        image: 'assets/img/product/sublime.png',
      },
      {
        class: 'absolute',
        description: 'HOME_PRODUCT_ABSOLUTE',
        image: 'assets/img/product/absolute.png',
      },
      {
        class: 'exclusive',
        description: 'HOME_PRODUCT_EXCLUSIVE',
        image: 'assets/img/product/exclusive.png',
      },
      {
        class: 'privilege',
        description: 'HOME_PRODUCT_PRIVILEGE',
        image: 'assets/img/product/privilege.png',
      },
    ];
  }

  ionViewDidLoad() {
    this.translateService.get([
      "ERROR_SCAN_BARCODE",
      "MESSAGE_BARCODE"
    ]).subscribe((values) => {
      this.translator = values;
    });
  }

  ionViewWillEnter() {
   this.longPressPageOpen = false;
  }

  async scanBarcode(){
    this.optionsBarcode = {
      showFlipCameraButton: true,
      showTorchButton: true,
      disableSuccessBeep: true,
      resultDisplayDuration: 0,
      prompt : this.translator.MESSAGE_BARCODE,
      formats: 'QR_CODE'
    }

    await this.barcodeScanner.scan(this.optionsBarcode).then((data) => {
      if (!data.cancelled) {
        this.openPage('CoffeeSamplesPage', {params: data.text})
      }
     }, (err) => {
      this.error.handleError(this.translator.ERROR_SCAN_BARCODE);
     });
  }

  protected openPage(namePage: string, params?: object) {
    this.navCtrl.push(namePage, params);
  }

  protected openPageLongPress(namePage: string) {
    if(!this.longPressPageOpen) {
      this.longPressPageOpen = true;
      this.navCtrl.push(namePage);
    }
  }
}

import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
//import { CallNumber } from '@ionic-native/call-number';
import { GoogleMapsService } from '../../providers/google-maps/google-maps';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  protected params: any;
  protected countNavs: number;

  constructor(
    //private callNumber: CallNumber,
    private maps: GoogleMapsService,
    private navCtrl: NavController
  ) {
    this.params = {
      map_center_coordinates: {
        lat: -19.000148,
        lng: -46.302865
      },
      text_marker: 'Veloso Coffee',
      zoomMap: 17,
      zoomControlMap: true
    }

    this.countNavs = this.navCtrl.length();
  }

  ionViewDidLoad() {
    this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement, this.params);
  }

  /*protected call() {
    this.callNumber.callNumber("+553438517100", false)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'))
  }*/

  protected homePage() {
    this.navCtrl.setRoot('HomePage');
  }
}

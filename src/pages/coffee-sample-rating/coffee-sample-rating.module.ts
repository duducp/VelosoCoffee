import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CoffeeSampleRatingPage } from './coffee-sample-rating';
import { Device } from '@ionic-native/device';

@NgModule({
  declarations: [
    CoffeeSampleRatingPage,
  ],
  imports: [
    IonicPageModule.forChild(CoffeeSampleRatingPage),
    TranslateModule.forChild(),
  ],
  providers: [
    Device
  ]
})
export class CoffeSampleRatingPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Range, AlertController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { Device } from '@ionic-native/device';

import { RatingSampleService } from '../../providers/sample-rating/sampleRatingService';
import { ErrorService } from '../../providers/error/error';

@IonicPage()
@Component({
  selector: 'page-coffee-sample-rating',
  templateUrl: 'coffee-sample-rating.html',
})
export class CoffeeSampleRatingPage {
  protected params: any;
  protected form: FormGroup;
  protected valueRange: number = 0;
  private ratings_samples = [];
  private translator: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private alertCtrl: AlertController,
    private fb: FormBuilder,
    private storage: Storage,
    private translateService: TranslateService,
    private error: ErrorService,
    private ratingSampleService: RatingSampleService,
    private device: Device
  ) {
    this.params = this.navParams.get('dataSample');

    this.form = fb.group({
			'acidity': [null, Validators.required],
			'sweetness': [null, Validators.required],
			'bitterness': [null, Validators.required],
			'aroma': [null, Validators.required],
			'body': [null, Validators.required]
    });

    this.translateService.get([
      "EVALUATE_SAMPLE_TITLE",
      "ERROR_SET_STORAGE",
      "ERROR_GET_STORAGE",
      "EVALUATE_SAMPLE_MSG_SEND_EVALUATION",
      "EVALUATE_SAMPLE_MSG_ERROR_SEND_EVALUATION",
      "BOTTON_OK"
    ]).subscribe((values) => {
      this.translator = values;
    });

    this.storage.get('rating_sample')
    .then(data => {
      if (data)
        this.ratings_samples = data;
    })
    .catch(err => {
      this.error.handleError(this.translator.ERROR_GET_STORAGE);
    });
  }

  protected rangeChange(range: Range) {
    this.valueRange = range.value;
  }

  protected submit() {
    var obj = {};
    obj["result"] = this.form.value;
    obj["idSample"] = this.params._id;
    obj["lang"] = this.translateService.currentLang;
    obj["uuid"] = this.device.uuid;

    this.ratingSampleService.post(obj).then((res) => {
      let resp: any = res;
      this.ratings_samples.unshift(resp.result)
      
      this.storage.set('rating_sample', this.ratings_samples)
      .then((res) => {
        this.showAlert(this.translator.EVALUATE_SAMPLE_MSG_SEND_EVALUATION);
        this.navCtrl.pop();
      })
      .catch(err => {
        this.error.handleError(this.translator.ERROR_SET_STORAGE);
      });

    }, (err) => {
      this.error.handleError(err);
      //this.showAlert(this.translator.EVALUATE_SAMPLE_MSG_ERROR_SEND_EVALUATION);
    });
  }

  private showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: this.translator.EVALUATE_SAMPLE_TITLE,
      subTitle: message,
      buttons: [this.translator.BOTTON_OK]
    });
    alert.present();
  }
}

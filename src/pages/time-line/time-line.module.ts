import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { TimeLinePage } from './time-line';

@NgModule({
  declarations: [
    TimeLinePage,
  ],
  imports: [
    IonicPageModule.forChild(TimeLinePage),
    TranslateModule.forChild()
  ],
})
export class TimeLinePageModule {}

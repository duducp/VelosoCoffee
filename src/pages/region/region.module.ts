import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { RegionPage } from './region';

@NgModule({
  declarations: [
    RegionPage,
  ],
  imports: [
    IonicPageModule.forChild(RegionPage),
    TranslateModule.forChild()
  ],
})
export class RegionPageModule {}

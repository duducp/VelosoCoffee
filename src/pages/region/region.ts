import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

export interface Slide {
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-region',
  templateUrl: 'region.html',
})
export class RegionPage {
  slides: Slide[];
  
  constructor(public navCtrl: NavController, translate: TranslateService) {
    translate.get([
      "REGION_SLIDE1_DESCRIPTION",
      "REGION_SLIDE2_DESCRIPTION",
      "REGION_SLIDE3_DESCRIPTION",
      "REGION_SLIDE4_DESCRIPTION",
    ]).subscribe(
      (values) => {
        this.slides = [
          {
            description: values.REGION_SLIDE1_DESCRIPTION,
            image: 'assets/img/region/1.jpg',
          },
          {
            description: values.REGION_SLIDE2_DESCRIPTION,
            image: 'assets/img/region/2.jpg',
          },
          {
            description: values.REGION_SLIDE3_DESCRIPTION,
            image: 'assets/img/region/3.jpg',
          },
          {
            description: values.REGION_SLIDE4_DESCRIPTION,
            image: 'assets/img/region/4.jpg',
          }
        ];
      });
  }

  next() {
    this.navCtrl.push('FarmPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}

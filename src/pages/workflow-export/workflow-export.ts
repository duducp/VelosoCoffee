import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-workflow-export',
  templateUrl: 'workflow-export.html',
})
export class WorkflowExportPage {

  constructor(public navCtrl: NavController) {
  }

}

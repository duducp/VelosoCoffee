import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkflowExportPage } from './workflow-export';

@NgModule({
  declarations: [
    WorkflowExportPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkflowExportPage),
    TranslateModule.forChild()
  ],
})
export class WorkflowExportPageModule {}

import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

import { MyApp } from '../../app/app.component';
import { ErrorService } from '../../providers/error/error';

export interface Language {
  code: string;
  language: string;
}

@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class OptionsPage {
  protected langs: any;
  protected currentLang: string;
  private errorSetStorage: string;

  constructor(
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private myApp: MyApp,
    private translateService: TranslateService,
    private storage: Storage,
    private error: ErrorService
  ){
    this.menuCtrl.close();
  }

  ionViewDidEnter() {
    this.translatePage();
  }

  private translatePage() {
    this.currentLang = this.translateService.currentLang;
    this.translateService.get([
      "LANGUAGES",
      "ERROR_SETSTORAGE_LANGUAGE"
    ]).subscribe((values) => {
      this.langs = values.LANGUAGES;
      this.errorSetStorage = values.ERROR_SETSTORAGE_LANGUAGE;
    });
  }

  protected selectLanguage(selected) {
    this.storage.set('config', {lang: selected.CODE})
    .then((res) => {
      this.myApp.initTranslate(selected.CODE);
    })
    .catch(err => {
      this.error.handleError(this.errorSetStorage);
    });
  }

  protected openPage(page: string){
    this.navCtrl.push(page);
  }

}

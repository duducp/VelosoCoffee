import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

export interface Product {
  class: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  protected products: Product[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.products = [
      {
        class: 'sublime',
        description: 'HOME_PRODUCT_SUBLIME',
        image: 'assets/img/product/sublime.png',
      },
      {
        class: 'absolute',
        description: 'HOME_PRODUCT_ABSOLUTE',
        image: 'assets/img/product/absolute.png',
      },
      {
        class: 'exclusive',
        description: 'HOME_PRODUCT_EXCLUSIVE',
        image: 'assets/img/product/exclusive.png',
      },
      {
        class: 'privilege',
        description: 'HOME_PRODUCT_PRIVILEGE',
        image: 'assets/img/product/privilege.png',
      },
    ];
  }

  next() {
    this.navCtrl.push('CertificationsPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}

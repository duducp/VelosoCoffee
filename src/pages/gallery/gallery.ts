import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { Storage } from '@ionic/storage';

import { InstagramApiProvider } from '../../providers/instagram-api/instagram-api';
import { ConnectivityService } from '../../providers/connectivity/connectivity';

@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {
  protected errorMessage: string;
  private photos: any;
  protected photosInfinitScroll = [];
  private localPhotosInfinitScroll: number = 0;
  protected displayType: boolean = true;
  protected profile;

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private instaAPI: InstagramApiProvider,
    private imageLoaderConfig: ImageLoaderConfig,
    private connectivityService: ConnectivityService
  ) {
    this.instaAPI = instaAPI;
  }

  ionViewDidLoad() {
    this.configImageLoader();

    if(this.connectivityService.isOnline()) {
      this.getInstaPhotos();
      this.getUserSelf();

    } else {
      this.storage.get('profileInstagram')
      .then((val) => {  
          this.profile = val;
      }, (err) => {
        console.log(err);
      });

      this.storage.get('galleryInstagram')
      .then((val) => {  
          this.forInitViewPhotos(val);
      }, (err) => {
        console.log(err);
      });
    }
  }

  protected doInfinite(infiniteScroll) {
    setTimeout(() => {
      for (let i = 0; i < 6 && (this.photos.length - this.localPhotosInfinitScroll) > i; i++) {
        this.photosInfinitScroll.push( this.photos[this.localPhotosInfinitScroll] );
        this.localPhotosInfinitScroll++;
      }

      infiniteScroll.complete();
    }, 500);
  }

  protected openPage(photo, index) {
    let obj = {
      photo: photo,
      index: index,
      photos: this.photos
    }
    this.navCtrl.push('GalleryViewPhotoPage', obj);
  }

  private getInstaPhotos() {
    this.instaAPI.getPhotos().subscribe(photos => {
      this.storage.set('galleryInstagram', photos);
      this.forInitViewPhotos(photos);

    }, err => {
      this.errorMessage = <any>err;
    });
  }

  private getUserSelf() {
    this.instaAPI.getUserSelf().subscribe(res => {
      this.profile = res;
      this.storage.set('profileInstagram', res);

    }, err => {
      this.errorMessage = <any>err;
    });
  }

  private forInitViewPhotos(photos: any) {
    this.photos = photos;

    for (var i = 0; i < 12 && photos.length > i; i++) {
      this.photosInfinitScroll.push( photos[i] );
      this.localPhotosInfinitScroll++;
    }
  }

  protected changeDisplayType(type: string) {
    if(type == 'grid')
      this.displayType = true;
    else
      this.displayType = false;
  }
  
  private configImageLoader() {
    this.imageLoaderConfig.enableSpinner(true); // enable spinner
    this.imageLoaderConfig.setConcurrency(15); // set the maximum concurrent connections
    this.imageLoaderConfig.setFallbackUrl('assets/img/fallback.jpg'); // if images fail to load, display this image instead
    this.imageLoaderConfig.useImageTag(true); // use `<img>` tag by default
  }
}

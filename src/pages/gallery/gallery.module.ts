import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { IonicImageLoader } from 'ionic-image-loader';

import { GalleryPage } from './gallery';

@NgModule({
  declarations: [GalleryPage],
  imports: [
    IonicImageLoader,
    IonicPageModule.forChild(GalleryPage),
    TranslateModule.forChild()
  ],
})
export class GalleryPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, AlertController, FabContainer, LoadingController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

import { SampleService } from './../../providers/sample-coffee/sampleCoffeeService';
import { ErrorService } from '../../providers/error/error';

@IonicPage()
@Component({
  selector: 'page-coffee-samples',
  templateUrl: 'coffee-samples.html',
})
export class CoffeeSamplesPage {
  private loading: any;
  protected samplesList = [];
  protected itemsInfinitScroll = [];
  protected localItemsInfinitScroll: number;
  protected translator: any;
  private optionsBarcode: BarcodeScannerOptions;

  constructor(
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private barcodeScanner: BarcodeScanner,
    private translateService: TranslateService,
    private navParams: NavParams,
    private storage: Storage,
    private error: ErrorService,
    private sampleService: SampleService) {
  }

  ionViewWillEnter() {
    this.menuCtrl.close();

    this.translateService.get([
      "COFFEE_SAMPLE_PROMPT_TITLE",
      "COFFEE_SAMPLE_PROMPT_MESSAGE",
      "COFFEE_SAMPLE_PROMPT_PLACEHOLDER",
      "COFFEE_SAMPLE_PROMPT_OK",
      "COFFEE_SAMPLE_PROMPT_CANCEL",
      "LOADING",
      "COFFEE_SAMPLE_DETAIL_ERROR_GETSTORAGE",
      "ERROR_SCAN_BARCODE",
      "COFFEE_SAMPLE_GET_REPEAT",
      "BOTTON_OK",
      "COFFEE_SAMPLE_TITLE",
      "MESSAGE_BARCODE"
    ]).subscribe((values) => {
      this.translator = values;
    });

    this.itemsInfinitScroll = [];
    this.localItemsInfinitScroll = 0;

    this.storage.get('samples')
    .then((val) => {
      if (val) {
        this.samplesList = val;

        for (var i = 0; i < 5 && this.samplesList.length > i; i++) {
          this.itemsInfinitScroll.push( this.samplesList[i] );
          this.localItemsInfinitScroll++;
        }
      }
    }, (err) => {
      this.error.handleError(this.translator.COFFEE_SAMPLE_DETAIL_ERROR_GETSTORAGE);
    });
  }

  ionViewDidEnter(){
    let params = this.navParams.get('params');
    if (params)
      this.getSampleCoffee(params)
  }

  protected async scanBarcode(fab: FabContainer){
    fab.close();

    this.optionsBarcode = {
      showFlipCameraButton: true,
      showTorchButton: true,
      disableSuccessBeep: true,
      resultDisplayDuration: 0,
      prompt : this.translator.MESSAGE_BARCODE,
      formats: 'QR_CODE'
    }

    await this.barcodeScanner.scan(this.optionsBarcode)
    .then((data) => {
      if (!data.cancelled) {
        this.getSampleCoffee(data.text);
      }
    }, (err) => {
      this.error.handleError(this.translator.ERROR_SCAN_BARCODE);
    });
  }

  protected showPrompt(fab: FabContainer) {
    let prompt = this.alertCtrl.create({
      title: this.translator.COFFEE_SAMPLE_PROMPT_TITLE,
      message: this.translator.COFFEE_SAMPLE_PROMPT_MESSAGE,
      inputs: [
        {
          name: 'codigo',
          placeholder: this.translator.COFFEE_SAMPLE_PROMPT_PLACEHOLDER
        },
      ],
      buttons: [
        {
          text: this.translator.COFFEE_SAMPLE_PROMPT_CANCEL,
          handler: data => {}
        },
        {
          text: this.translator.COFFEE_SAMPLE_PROMPT_OK,
          handler: data => {
            this.getSampleCoffee(data.codigo);
          }
        }
      ]
    });
    prompt.present();

    prompt.onDidDismiss(() => {
      fab.close();
    });
  }

  private getSampleCoffee(code: String){
    this.showLoader();

    this.sampleService.get(code).then((res) => {

      let repeat = false;
      for (var i in this.samplesList) {
        if (this.samplesList[i].code == res[0].code) {
          this.samplesList[i] = res[0]; //Atualiza o objeto repetido

          this.showAlert(this.translator.COFFEE_SAMPLE_GET_REPEAT);
          repeat = true;
          break;
        }
      }

      if (!repeat) {
        this.samplesList.unshift(res[0]);
        this.itemsInfinitScroll.unshift(res[0]);
        this.localItemsInfinitScroll++;
      }

      this.storage.set('samples', this.samplesList);
      this.loading.dismiss();
    }, (err) => {
      this.error.handleError(err);
      this.loading.dismiss();
    });
  }

  protected doInfinite(infiniteScroll) {
    setTimeout(() => {
      for (let i = 0; i < 5 && (this.samplesList.length - this.localItemsInfinitScroll) > i; i++) {
        this.itemsInfinitScroll.push( this.samplesList[this.localItemsInfinitScroll] );
        this.localItemsInfinitScroll++;
      }

      infiniteScroll.complete();
    }, 500);
  }

  private showLoader() {
    this.loading = this.loadingCtrl.create({
      content: this.translator.LOADING
    });

    this.loading.present();
  }

  private showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: this.translator.COFFEE_SAMPLE_TITLE,
      subTitle: message,
      buttons: [this.translator.BOTTON_OK]
    });
    alert.present();
  }

  protected openPage(data: any, index: number){
    this.navCtrl.push("CoffeeSamplesDetailsPage", {sample: data, index: index});
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CoffeeSamplesPage } from './coffee-samples';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SampleService } from './../../providers/sample-coffee/sampleCoffeeService';

@NgModule({
  declarations: [
    CoffeeSamplesPage,
  ],
  imports: [
    IonicPageModule.forChild(CoffeeSamplesPage),
    TranslateModule.forChild()
  ],
  providers: [
    BarcodeScanner,
    SampleService
  ]
})
export class CoffeeSamplesPageModule {}

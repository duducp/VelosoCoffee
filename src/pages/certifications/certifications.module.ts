import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CertificationsPage } from './certifications';

@NgModule({
  declarations: [
    CertificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(CertificationsPage),
    TranslateModule.forChild()
  ],
})
export class CertificationsPageModule {}

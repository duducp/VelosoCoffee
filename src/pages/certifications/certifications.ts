import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

export interface Certifications {
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-certifications',
  templateUrl: 'certifications.html',
})
export class CertificationsPage {
  protected certifications: Certifications[];

  constructor(public navCtrl: NavController) {
    this.certifications = [
      {
        description: 'CERTIFICATIONS_BSCA',
        image: 'assets/img/certifications/bsca.png',
      },
      {
        description: 'CERTIFICATIONS_RCM',
        image: 'assets/img/certifications/rcm.png',
      },
      {
        description: 'CERTIFICATIONS_RAC',
        image: 'assets/img/certifications/rac.png',
      },
      {
        description: 'CERTIFICATIONS_UTZ',
        image: 'assets/img/certifications/utz.png',
      },
    ];
  }

  next() {
    this.navCtrl.push('ContactPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}

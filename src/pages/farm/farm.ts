import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

export interface Card {
  title: string;
  subtitle: string;
  subtitle2: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-farm',
  templateUrl: 'farm.html',
})
export class FarmPage {
  cards: Card[];

  constructor(public navCtrl: NavController) {
    this.cards = [
      {
        title: 'FARM_FARM_TITLE1',
        subtitle: 'FARM_FARM_CITY1',
        subtitle2: 'FARM_FARM_REGION',
        image: 'assets/img/farm/st-cecilia.jpg',
      },
      {
        title: 'FARM_FARM_TITLE2',
        subtitle: 'FARM_FARM_CITY2',
        subtitle2: 'FARM_FARM_REGION',
        image: 'assets/img/farm/st-rita.jpg',
      },
      {
        title: 'FARM_FARM_TITLE3',
        subtitle: 'FARM_FARM_CITY3',
        subtitle2: 'FARM_FARM_REGION',
        image: 'assets/img/farm/palmeira.jpg',
      }
    ];
  }

  next() {
    this.navCtrl.push('WorkflowPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
}

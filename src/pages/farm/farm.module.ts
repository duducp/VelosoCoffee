import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { FarmPage } from './farm';

@NgModule({
  declarations: [
    FarmPage,
  ],
  imports: [
    IonicPageModule.forChild(FarmPage),
    TranslateModule.forChild()
  ],
})
export class FarmPageModule {}

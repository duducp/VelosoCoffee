import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-coffee-sample-rating-view',
  templateUrl: 'coffee-sample-rating-view.html',
})
export class CoffeeSampleRatingViewPage {
  protected params: any;
  protected dataRating: any;

  constructor(private navParams: NavParams) {
    this.params = this.navParams.get('dataSample');
    this.dataRating = this.navParams.get('dataRating');
  }
}

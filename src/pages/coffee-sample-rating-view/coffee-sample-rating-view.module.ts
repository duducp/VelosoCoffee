import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { CoffeeSampleRatingViewPage } from './coffee-sample-rating-view';

@NgModule({
  declarations: [
    CoffeeSampleRatingViewPage,
  ],
  imports: [
    IonicPageModule.forChild(CoffeeSampleRatingViewPage),
    TranslateModule.forChild(),
  ],
})
export class CoffeSampleRatingViewPageModule {}

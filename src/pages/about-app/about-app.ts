import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-about-app',
  templateUrl: 'about-app.html',
})
export class AboutAppPage {
  protected version: any;
  protected name: any;
  protected date = moment().format();

  constructor(private appVersion: AppVersion) {
    this.appVersion.getAppName().then((res) => {
      this.name = res;
    }, (err) => {
      console.log(err);
    });

    this.appVersion.getVersionNumber().then((res) => {
      this.version = res;
    }, (err) => {
      console.log(err);
    });
  }

}

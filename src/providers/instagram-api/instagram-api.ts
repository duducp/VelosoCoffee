import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Jsonp, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AppConfig } from '../app-config';

@Injectable()
export class InstagramApiProvider {

  constructor (private jsonp: Jsonp, private config: AppConfig) {}

  private instaURL: string = 'https://api.instagram.com/';

  public getPhotos(): Observable<any> {
    return this.jsonp.get(this.instaURL + 
      'v1/users/' + this.config.apiInstagramIdUser +
      '/media/recent/' +
      '?count=99' +
      '&callback=JSONP_CALLBACK' +
      '&access_token=' + this.config.apiInstagramToken)
      .map(this.extractData)
      .catch(this.handleError);
  }

  public getUserSelf(): Observable<any> {
    return this.jsonp.get(this.instaURL + 
      'v1/users/self/' +
      '?callback=JSONP_CALLBACK' +
      '&access_token=' + this.config.apiInstagramToken)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}

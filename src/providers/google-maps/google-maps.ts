import { Injectable } from '@angular/core';
import { ConnectivityService } from '../connectivity/connectivity';
import { TranslateService } from '@ngx-translate/core';
 
declare var google;

@Injectable()
export class GoogleMapsService {
 
  private mapElement: any;
  private pleaseConnect: any;
  private map: any;
  private mapInitialised: boolean = false;
  private dataCoords: any;
  private apiKey: string = "AIzaSyCHdbLDEBgzJbdTYTgjMHf4DSoar3PQLw0";
  private language: any;
 
  constructor(
    private connectivityService: ConnectivityService,
    private translateService: TranslateService) {
    
  }
 
  public init(mapElement: any, pleaseConnect: any, dataCoords: any): Promise<any> {
    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;
    this.dataCoords = dataCoords;
    this.language = this.translateService.currentLang;

    return this.loadGoogleMaps();
  }
 
  private loadGoogleMaps(): Promise<any> {
    //http://www.birdtheme.org/useful/v3tool.html
    //https://codepen.io/jhawes/pen/ujdgK?editors=0010

    return new Promise((resolve) => {
      if(typeof google == "undefined" || typeof google.maps == "undefined"){
        this.disableMap();
 
        if(this.connectivityService.isOnline()) {
          window['mapInit'] = () => {
            this.initMap(this.dataCoords).then(() => {
              resolve(true);
            });
 
            this.enableMap();
          }
 
          let script = document.createElement("script");
          script.id = "googleMaps";
 
          if(this.apiKey){
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&language=' + this.language +'&callback=mapInit&libraries=places';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?language=' + this.language +'&callback=mapInit';       
          }
 
          document.body.appendChild(script);
        } 
      } else {
        if(this.connectivityService.isOnline()){
          this.initMap(this.dataCoords);
          this.enableMap();
        }
        else {
          this.disableMap();
        }

        resolve(true);
      }
 
      this.addConnectivityListeners();
    });
 
  }
 
  private initMap(data: any): Promise<any> {
    this.mapInitialised = true;
    let zoomMap: number = data.zoomMap ? data.zoomMap : 15;
    let zoomControlMap: boolean = data.zoomControlMap ? data.zoomControlMap : false;

    return new Promise((resolve) => {
      // Map Center
      let latLng = new google.maps.LatLng(
        data.map_center_coordinates.lat,
        data.map_center_coordinates.lng
      );

      // General Options
      let mapOptions = {
        zoom: zoomMap,
        center: latLng,
        //disableDefaultUI: true, // desabilita todos os botões do mapa
        scrollwheel: false, // zoom com scroll
        draggable: true, // mover o mapa com o scroll
        zoomControl: zoomControlMap,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.LARGE 
        },
        mapTypeControl: false,
        mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID]
        },
        scaleControl: false,
        streetViewControl: false,
        rotateControl: true,
        fullscreenControl: true,
        mapTypeId: google.maps.MapTypeId.SATELLITE
      }

      // Set Map
      this.map = new google.maps.Map(this.mapElement, mapOptions);
 
      // Polygon Coordinates
      if(data.map_polygon_coordinates)
        this.addPolygon(data);

      // Styling and Add Marker   
      if (data.text_marker) {
        this.addMarker(latLng, data.text_marker, data.iconMap ? data.iconMap : 'assets/icon/gmaps.png');
      }

      resolve(true);
    });
  }

  private addPolygon(data: any) {
    let myCoordinates = [];
    for(let i = 0; i < data.map_polygon_coordinates.length; i++) {
      myCoordinates.push(new google.maps.LatLng(data.map_polygon_coordinates[i].lat, data.map_polygon_coordinates[i].lng));
    }

    // Styling & Controls Polygon
    let polyOptions = new google.maps.Polygon({
      paths: myCoordinates,
      draggable: false, // turn off if it gets annoying
      editable: false,
      strokeColor: '#7DB044',
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: '#7DB044',
      fillOpacity: 0.1
    });
  
    polyOptions.setMap(this.map);
  }

  private addMarker(latLng: any, text_marker: string, icon: string){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      icon: icon
    });
  
    let content = "<p><b>" + text_marker + "</b></p>";

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
  
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }
 
  private disableMap(): void {
    if(this.pleaseConnect) {
      this.pleaseConnect.style.display = "block";
    }
  }
 
  private enableMap(): void {
    if(this.pleaseConnect) {
      this.pleaseConnect.style.display = "none";
    }
  }
 
  private addConnectivityListeners(): void {
    this.connectivityService.watchOnline().subscribe(() => {
      setTimeout(() => {
        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        } 
        else {
          if(!this.mapInitialised){
            this.initMap(this.dataCoords);
          }
          this.enableMap();
        }
      }, 2000);
    });
 
    this.connectivityService.watchOffline().subscribe(() => {
      this.disableMap();
    });
  }
 
}
import { NgModule } from '@angular/core';
import { Api } from './api';
import { AppConfig } from './app-config';
import { ErrorService } from './error/error';
import { GoogleMapsService } from './google-maps/google-maps';
import { ConnectivityService } from './connectivity/connectivity';
import { SampleService } from './sample-coffee/sampleCoffeeService';
import { RatingSampleService } from './sample-rating/sampleRatingService';
import { InstagramApiProvider } from './instagram-api/instagram-api';
import { JsonpModule } from '@angular/http';

@NgModule({
    imports: [
        JsonpModule
    ],
    providers: [
        AppConfig,
        Api,
        ErrorService,
        GoogleMapsService,
        ConnectivityService,
        SampleService,
        RatingSampleService,
        InstagramApiProvider
    ]
})
export class ServicesModule {
}
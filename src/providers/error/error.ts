import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ErrorService {

  constructor(private toastCtrl: ToastController, private translateService: TranslateService) {
  }

  private getTranslator(): Promise<any> {
    return new Promise((resolve) => {
      this.translateService.get([
        "ERROR_TIMEOUT",
        "ERROR_SERVER"
      ]).subscribe((values) => {
        resolve(values);
      });
    });
  }

  public handleError (error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);

      if (error.status == 0) {
        this.getTranslator().then(data => {
          this.presentToast(data.ERROR_SERVER);
        })
      }

      //errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errMsg = JSON.parse(err).message;
      this.presentToast(errMsg);
    } else {

      if (error.name == "TimeoutError") {
        this.getTranslator().then(data => {
          this.presentToast(data.ERROR_TIMEOUT);
        })
      } else {
        errMsg = error.error.errorCode + ' - ' + error.error.message
        this.presentToast(errMsg);
      }

    }
  }

  private presentToast(message: string, duration: number = 3000, position: string = 'bottom') {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

}

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Api } from '../api';
import { TranslateService } from '@ngx-translate/core';
import { ConnectivityService } from '../connectivity/connectivity';

@Injectable()
export class RatingSampleService {
  private translator: any;

  constructor(
    private api: Api,
    private translateService: TranslateService,
    private connectivityService: ConnectivityService,
  ) {
    this.translateService.get([
      "ERROR_DISCONNECTED"
    ]).subscribe((values) => {
      this.translator = values;
    });
  }

  public post(body: any) {
    return new Promise((resolve, reject) => {
      if(this.connectivityService.isOnline()) {

        let seq = this.api.post('sample/rating/', body, {
          lang: this.translateService.currentLang
        }).share();
        
        seq
          .timeout(15000)
          .subscribe(res => {
            resolve(res);
          }, err => {
            reject(err);
          });

      } else {
        reject(this.translator.ERROR_DISCONNECTED);
      }
    });
  }

}

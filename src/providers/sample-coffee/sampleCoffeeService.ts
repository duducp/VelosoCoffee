import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Api } from '../api';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { ConnectivityService } from '../connectivity/connectivity';

@Injectable()
export class SampleService {

  private translator: any;

  constructor(
    private api: Api,
    private translateService: TranslateService,
    private connectivityService: ConnectivityService
  ) {
    this.translateService.get([
      "ERROR_DISCONNECTED"
    ]).subscribe((values) => {
      this.translator = values;
    });
  }

  public get(code: String): Promise<any> {
    return new Promise((resolve, reject) => {
      if(this.connectivityService.isOnline()) {

        let seq = this.api.get('sample/coffee/', {
          key: 'code', text: code,
          lang: this.translateService.currentLang
        }).share();
        
        seq
          .timeout(15000)
          .subscribe( res => {
            resolve(res);
          }, err => {
            reject(err);
          });

      } else {
        reject(this.translator.ERROR_DISCONNECTED);
      }
    });
  }
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';

import { AppConfig } from './app-config';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  private url: any;

  constructor(public http: HttpClient, public appConfig: AppConfig) {
    this.url = this.appConfig.apiEndpoint;
  }

  get(endpoint: string, params?: any) {    
    let httpParams = new HttpParams();

    // Support easy query params for GET requests
    if (params) {
      Object.keys(params).forEach(function (key) {
        httpParams = httpParams.append(key, params[key]);
      });
    }

    return this.http.get(this.url + '/' + endpoint, {params: httpParams});
  }

  post(endpoint: string, body: any, params?: any) {
    let httpParams = new HttpParams();

    if (params) {
      Object.keys(params).forEach(function (key) {
        httpParams = httpParams.append(key, params[key]);
      });
    }

    return this.http.post(this.url + '/' + endpoint, body, {params: httpParams});
  }

  put(endpoint: string, body: any) {
    return this.http.put(this.url + '/' + endpoint, body);
  }

  delete(endpoint: string) {
    return this.http.delete(this.url + '/' + endpoint);
  }

  patch(endpoint: string, body: any) {
    return this.http.put(this.url + '/' + endpoint, body);
  }
}

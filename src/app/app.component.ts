import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Config } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Globalization } from '@ionic-native/globalization';
import { Storage } from '@ionic/storage';

import { TranslateService } from '@ngx-translate/core';
import { ErrorService } from '../providers/error/error';
import { ConnectivityService } from '../providers/connectivity/connectivity';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) protected nav: Nav;

  public online: boolean = false;
  protected rootPage: string = 'HomePage';
  protected activePage: any;
  protected pages: Array<{ title: string, icon?: string, component: string, setRoot?: boolean }>;

  constructor(
    private translate: TranslateService,
    private platform: Platform,
    private config: Config,
    private globalization: Globalization,
    private statusBar: StatusBar,
    private error: ErrorService,
    private splashScreen: SplashScreen,
    private storage: Storage,
    private network: ConnectivityService
  ) {
    this.translate.setDefaultLang('pt');
    this.initializeApp();
  }

  private initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.verifyStorageLanguage();
    });
  }

  private verifyStorageLanguage() {
    this.storage.get('config')
    .then(data => {
      if (data)
        this.initTranslate(data.lang);
      else
        this.globalizationLanguage();
    })
    .catch(err => {
      console.log(err);
    });
  }

  private globalizationLanguage() {
    this.globalization.getLocaleName()
    .then(res => {
        this.initTranslate(res.value.slice(0, 2));
    })
    .catch(e => {
      this.error.handleError(e);
    });
  }

  public initTranslate(language: string) {
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    this.platform.setLang(language, true);

    this.translate.get(['BACK_BUTTON_TEXT', 'MENU']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
      this.initMenu(values.MENU);
    });

    this.storage.set('config', {lang: language});
    this.verifyNetwork();
    this.hideSplashScreen();
  }

  private initMenu(menu: any) {
    this.pages = [
      {title: menu.TIMELINE, component: 'TimeLinePage'},
      {title: menu.HISTORY, component: 'HistoryPage'},
      {title: menu.REGION, component: 'RegionPage'},
      {title: menu.FARM, component: 'FarmPage'},
      {title: menu.WORKFLOW, component: 'WorkflowPage'},
      {title: menu.PRODUCTS, component: 'ProductsPage'},
      {title: menu.CERTIFICATIONS, component: 'CertificationsPage'},
      {title: menu.CONTACT, component: 'ContactPage'},
      {title: menu.GALLERY, component: 'GalleryPage'}
    ];
    //this.activePage = this.pages[0];
  }

  private hideSplashScreen() {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 100);
  }

  protected openPage(page) {
    page.setRoot == true ? this.nav.setRoot(page.component) : this.nav.push(page.component);
    this.activePage = page;
  }

  protected checkActive(page) {
    return page == this.activePage;
  }

  private verifyNetwork() {
    if (this.network.isOnline())
      this.online = true;

    this.network.watchOffline().subscribe(() => {
      this.online = false;
    });

    this.network.watchOnline().subscribe(() => {
      this.online = true;
    });
  }
}
